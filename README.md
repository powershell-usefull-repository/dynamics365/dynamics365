# Dynamics365 Finance and Operation module

Module designed for Dynamics 365 Finance and Operation CRM application.
These commands are based on odata APIs.

## Prerequisites

You need to create an application into Azure AD, which will be used for authentication. Usage of standard authentication using '00000015-0000-0000-c000-000000000000'app ID is in study.

## Available commands :

- Connect-DynamicsFO365 : Initiate connection to AzureAD
- Get-DynamicsFO365SystemUsers : Return list of system users (can take userID as filter param)
- Get-DynamicsFO365PersonUsers : Return list of person users
- Get-DynamicsFO365DataManagementExecutionJobDetails : return list of Data Management import/export executions


## Installation


## Usage


## Support
Do not hesitate to open issue in case of.

## Roadmap


## Contributing


## Authors and acknowledgment


## License
For open source projects, say how it is licensed.

## Project status
Commands are in development.